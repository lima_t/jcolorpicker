/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.wincolorpicker;

import application.wincolorpicker.forms.TelaPrincipal;
import javax.swing.JOptionPane;

/**
 *
 * @author whoIAm
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try{
        
            new TelaPrincipal().setVisible(true);
            
        }catch(Exception erro){
            JOptionPane.showMessageDialog(
                    null, 
                    "Não foi possível iniciar o programa devido a um erro fatal.\n[Erro]:\n"+erro.getMessage(), 
                    "Erro Fatal", 
                    JOptionPane.ERROR_MESSAGE);
        }
    }
    
}
