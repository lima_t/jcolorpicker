/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.wincolorpicker.utils;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

/**
 *
 * @author whoIAm
 */
public class ColorUtils {
    
    public static String colorToHexaDecimal(Color cor){
        String hexadecimal = Integer.toHexString(cor.getRGB());
        return hexadecimal.substring(2, hexadecimal.length());
    }
    
    public static String colorToRgb(Color cor){
        String rgbString = cor.getRed()+", ";
        rgbString += cor.getGreen()+", ";
        rgbString += cor.getBlue();
        return rgbString;
    }
    
    public static void copyTextToClipboard(String textoCopiado){
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        StringSelection selecaoTxt = new StringSelection(textoCopiado);
        clipboard.setContents(selecaoTxt, null);
    }
}
