package application.wincolorpicker.utils;

import application.wincolorpicker.interfaces.EyeDropperToolListener;
import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.Set;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JWindow;

/**
 *
 * @author whoIAm
 */
public class EyeDropperTool{
    
    private final   Dimension dimensions;
    private         Robot robot;
    private         Color cor;
    private         EyeDropperToolListener listener;
    
    public EyeDropperTool() throws Exception{
        // Pega as dimensões da tela
        dimensions = Toolkit.getDefaultToolkit().getScreenSize();

        // Captura a tela:
        robot = new Robot();
        BufferedImage img = robot.createScreenCapture(new Rectangle(dimensions));

        //Cria um PrintScreen em memória e inicia os componentes
        JWindow janela = new JWindow();
        Container container = janela.getContentPane();
        container.setLayout(new BorderLayout());
        JLabel label = new JLabel(new ImageIcon(img));
        container.add(label, BorderLayout.CENTER);
        Cursor cursor = new Cursor(Cursor.CROSSHAIR_CURSOR);
        container.setCursor(cursor);
        janela.setSize(dimensions);
        janela.setVisible(true);
        
        //Criando o evento de captura do mouse
        label.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event){
                try{
                    listener.onColorPicked(robot.getPixelColor(event.getX(), event.getY()));                    
                }catch(Exception erro){
                    throw erro;
                }finally{
                    janela.setVisible(false);
                }
            }
        });
    }
    
    public void addEyeDropperToolListener(EyeDropperToolListener listener){
        this.listener = listener;
    }
}
