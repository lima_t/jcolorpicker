package application.wincolorpicker.forms;

import application.wincolorpicker.interfaces.EyeDropperToolListener;
import application.wincolorpicker.utils.ColorUtils;
import application.wincolorpicker.utils.EyeDropperTool;
import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author whoIAm
 */
public class TelaPrincipal extends javax.swing.JFrame {
    
    public TelaPrincipal() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.colorPicker.getSelectionModel().addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                txtCorHexaDecimal.setText(ColorUtils.colorToHexaDecimal(colorPicker.getColor()).toUpperCase());
                txtCorRgb.setText(ColorUtils.colorToRgb(colorPicker.getColor()));
            }
        });
    }
    
    private Image setFormIcon(){
        try{
            URL urlImagemTitulo = this.getClass().getResource("/application/wincolorpicker/images/color-picker.png");
            Image imagemTitulo = Toolkit.getDefaultToolkit().getImage(urlImagemTitulo);
            return imagemTitulo;
        }catch(Exception erro){
            JOptionPane.showMessageDialog(
                    this,
                    "Não foi possivel carregar o icone da janela.\n[Erro]:\n\n"+erro,
                    "Erro", 
                    JOptionPane.ERROR_MESSAGE);
            return null;
        }

    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        colorPicker = new javax.swing.JColorChooser();
        painelPropriedades = new javax.swing.JPanel();
        txtCorHexaDecimal = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtCorRgb = new javax.swing.JTextField();
        btCopiarHexa = new javax.swing.JButton();
        btCopiarRgb = new javax.swing.JButton();
        btLimpar = new javax.swing.JButton();
        btSeletor = new javax.swing.JButton();
        barraMenu = new javax.swing.JMenuBar();
        menuArquivo = new javax.swing.JMenu();
        itemSair = new javax.swing.JMenuItem();
        menuEditar = new javax.swing.JMenu();
        itemCopiarCor = new javax.swing.JMenuItem();
        itemCopiarRgb = new javax.swing.JMenuItem();
        menuAjuda = new javax.swing.JMenu();
        itemProjeto = new javax.swing.JMenuItem();
        itemSobre = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Windows Color Picker");
        setIconImage(setFormIcon());
        setResizable(false);

        painelPropriedades.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Propriedades", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 11))); // NOI18N

        jLabel1.setText("Hexadecimal:");

        jLabel2.setText("RGB:");

        btCopiarHexa.setText("Copiar");
        btCopiarHexa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCopiarHexaActionPerformed(evt);
            }
        });

        btCopiarRgb.setText("Copiar");
        btCopiarRgb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCopiarRgbActionPerformed(evt);
            }
        });

        btLimpar.setText("Limpar");
        btLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btLimparActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout painelPropriedadesLayout = new javax.swing.GroupLayout(painelPropriedades);
        painelPropriedades.setLayout(painelPropriedadesLayout);
        painelPropriedadesLayout.setHorizontalGroup(
            painelPropriedadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelPropriedadesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelPropriedadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelPropriedadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtCorRgb, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCorHexaDecimal, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(painelPropriedadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btCopiarRgb, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btCopiarHexa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btLimpar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        painelPropriedadesLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btCopiarHexa, btCopiarRgb, btLimpar});

        painelPropriedadesLayout.setVerticalGroup(
            painelPropriedadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelPropriedadesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelPropriedadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCorHexaDecimal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(btCopiarHexa))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelPropriedadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtCorRgb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btLimpar)
                    .addComponent(btCopiarRgb))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btSeletor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/application/wincolorpicker/images/color-picker24x24.png"))); // NOI18N
        btSeletor.setText("Seletor");
        btSeletor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSeletorActionPerformed(evt);
            }
        });

        menuArquivo.setText("Arquivo");

        itemSair.setText("Sair");
        itemSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemSairActionPerformed(evt);
            }
        });
        menuArquivo.add(itemSair);

        barraMenu.add(menuArquivo);

        menuEditar.setText("Editar");

        itemCopiarCor.setText("Copiar Cor Hexadecimal");
        itemCopiarCor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemCopiarCorActionPerformed(evt);
            }
        });
        menuEditar.add(itemCopiarCor);

        itemCopiarRgb.setText("Copiar Cor RGB");
        itemCopiarRgb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemCopiarRgbActionPerformed(evt);
            }
        });
        menuEditar.add(itemCopiarRgb);

        barraMenu.add(menuEditar);

        menuAjuda.setText("Ajuda");

        itemProjeto.setText("Projeto");
        menuAjuda.add(itemProjeto);

        itemSobre.setText("Sobre");
        itemSobre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemSobreActionPerformed(evt);
            }
        });
        menuAjuda.add(itemSobre);

        barraMenu.add(menuAjuda);

        setJMenuBar(barraMenu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(colorPicker, javax.swing.GroupLayout.PREFERRED_SIZE, 614, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(painelPropriedades, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btSeletor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(colorPicker, javax.swing.GroupLayout.PREFERRED_SIZE, 311, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(painelPropriedades, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btSeletor, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(40, 40, 40))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btCopiarHexaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCopiarHexaActionPerformed
        if(txtCorHexaDecimal.getText() != null){
            ColorUtils.copyTextToClipboard(txtCorHexaDecimal.getText());
        }
    }//GEN-LAST:event_btCopiarHexaActionPerformed

    private void btCopiarRgbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCopiarRgbActionPerformed
        if(txtCorRgb.getText() != null){
            ColorUtils.copyTextToClipboard(txtCorRgb.getText());
        }
    }//GEN-LAST:event_btCopiarRgbActionPerformed

    private void btLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btLimparActionPerformed
        txtCorHexaDecimal.setText("");
        txtCorRgb.setText("");
    }//GEN-LAST:event_btLimparActionPerformed

    private void btSeletorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSeletorActionPerformed
        try{
            this.setVisible(false);
            
            Thread.sleep(300);
            
            EyeDropperTool seletor = new EyeDropperTool();
            seletor.addEyeDropperToolListener((Color cor) -> {
                colorPicker.setColor(cor);
                txtCorHexaDecimal.setText(ColorUtils.colorToHexaDecimal(cor).toUpperCase());
                txtCorRgb.setText(ColorUtils.colorToRgb(cor));
                this.setVisible(true);
            });
                        
        }catch(Exception erro){
            JOptionPane.showMessageDialog(
                    null, 
                    "Não foi possível iniciar a ferramenta de seletor.\n[Erro]:\n\n"+erro.getMessage(), 
                    "Erro", 
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btSeletorActionPerformed

    private void itemSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemSairActionPerformed
        int cod = JOptionPane.showConfirmDialog(
                    this, 
                    "Você realmente deseja sair do programa?", 
                    "Confirmar", 
                    JOptionPane.YES_NO_OPTION, 
                    JOptionPane.QUESTION_MESSAGE);
        
        if(cod == JOptionPane.YES_OPTION){
            System.exit(0);
        }
    }//GEN-LAST:event_itemSairActionPerformed

    private void itemCopiarCorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemCopiarCorActionPerformed
        if(txtCorHexaDecimal.getText() != null){
            ColorUtils.copyTextToClipboard(txtCorHexaDecimal.getText());
        }
    }//GEN-LAST:event_itemCopiarCorActionPerformed

    private void itemCopiarRgbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemCopiarRgbActionPerformed
        if(txtCorRgb.getText() != null){
            ColorUtils.copyTextToClipboard(txtCorRgb.getText());
        }
    }//GEN-LAST:event_itemCopiarRgbActionPerformed

    private void itemSobreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemSobreActionPerformed
        new TelaSobre(this, true).setVisible(true);
    }//GEN-LAST:event_itemSobreActionPerformed
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    protected javax.swing.JMenuBar barraMenu;
    private javax.swing.JButton btCopiarHexa;
    private javax.swing.JButton btCopiarRgb;
    private javax.swing.JButton btLimpar;
    private javax.swing.JButton btSeletor;
    protected javax.swing.JColorChooser colorPicker;
    private javax.swing.JMenuItem itemCopiarCor;
    private javax.swing.JMenuItem itemCopiarRgb;
    private javax.swing.JMenuItem itemProjeto;
    private javax.swing.JMenuItem itemSair;
    private javax.swing.JMenuItem itemSobre;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu menuAjuda;
    private javax.swing.JMenu menuArquivo;
    private javax.swing.JMenu menuEditar;
    protected javax.swing.JPanel painelPropriedades;
    private javax.swing.JTextField txtCorHexaDecimal;
    private javax.swing.JTextField txtCorRgb;
    // End of variables declaration//GEN-END:variables
}
