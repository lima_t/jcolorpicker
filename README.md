
# JColorPicker

## 1. Resumo
Este projeto é um utilitário (color-picker) para captura de cores de um bitmap que pode ser obtido com uma captura de tela do sistema. A ferramenta foi desenvolvida durante o ano de **2012** com o objetivo de suplementar a plataforma Windows durante o uso de ferramentas gráficas da época como Adobe Fireworks que não permitia efetuar a captura de cores fora da ferramenta própria. Este projeto foi versionado recentemente no GIT, para apoiar o portfólio de conhecimento, mas trata-se de um aplicativo antigo.

## 2. Requisitos Funcionais
A solução deve contemplar as seguintes funcionalidades:

- Aplicação deverá rodar em todos os sistemas operacionais (Windows e Linux);
- Deverá trabalhar com o conceito de bibliotecas no Java Swing;
- A aplicação deverá efetuar a captura de tela do sistema operacional e posteriormente abrir o dialog para seleção da cor com o conta-gotas;
- A cor selecionada deverá ser representada em suas possíveis codificações (RGB, Hexadecimal, etc.)


## 3. Dependências

O que você precisa para configurar e executar esta aplicação no ambiente local?

- [Java 7 ou superior](https://www.oracle.com/br/java/technologies/javase/javase7-archive-downloads.html)
- [Netbeans 7.1 ou superior](https://netbeans.apache.org/download/nb121/nb121.html)
- [Git SCM](https://git-scm.com/downloads)
- [SourceTree](https://www.sourcetreeapp.com/)

---

## 4. Tecnologias usadas no Projeto

Neste projeto foram utilizadas as seguintes tecnologias/frameworks.

- **Java Swing** para o desenvolvimento das telinhas, iterações e funcionalidades em geral;
- **Corel Draw** para edição dos ícones/redimensionamento.

---

## 5. Executando o Projeto

Para configurar o projeto e executar no ambiente local de desenvolvimento siga os passos:

1. Importar o projeto no Netbeans ou Eclipse compatível com Java SE
3. Executar o projeto como Java Swing (Em versões mais modernas do java configurar o compilador no modo compatibilidade do java 7)

## 6. Download do executável publicado

Não disponível para esse projeto

## Referências

Alguns artigos que foram usados como referência para estudo e construção desse projeto:

1. Conhecimento adquirido no período.

## Sobre o Autor

Tarcísio de Lima Amorim <br>
Email: tarcisio.lima.amorim@outlook.com <br>
Linkedin: https://www.linkedin.com/in/tarcisio-lima/